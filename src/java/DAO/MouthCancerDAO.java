/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import static DAO.BloodCancerDAO.session;
import Utils.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Transaction;
import pojos.Bloodcancerdoctors;
import pojos.Mouthcancerdoctors;

/**
 *
 * @author Harsh Jasani
 */
public class MouthCancerDAO {
    
    public static List<Mouthcancerdoctors> Search()
    {
        session=HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        Query q=session.createQuery("from Mouthcancerdoctors");
        List<Mouthcancerdoctors> list = q.list();
        t.commit();
        session.close();
        return list;
    }
    
}
