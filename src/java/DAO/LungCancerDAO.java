/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import static DAO.BloodCancerDAO.session;
import Utils.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Transaction;
import pojos.Bonecancerdoctors;
import pojos.Lungcancerdoctors;
import pojos.Mouthcancerdoctors;

/**
 *
 * @author Harsh Jasani
 */
public class LungCancerDAO {
       public static List<Lungcancerdoctors> Search()
    {
        session=HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        Query q=session.createQuery("from Lungcancerdoctors");
        List<Lungcancerdoctors> list = q.list();
        t.commit();
        session.close();
        return list;
    }
}
