    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Utils.HibernateUtil;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojos.Registration;


/**
 *
 * @author Harsh Jasani
 */
public class RegistationDAO {
    
    static Session session;
    
    public static void Save(Registration r)
    {
        session = HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        session.save(r);
        t.commit();
        session.close();
    }
    
    public static int Update(Registration r)
    {
        session = HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        Query q=session.createQuery("update Registration set password=:p where email=:e");
        q.setParameter("p", r.getPassword());
        q.setParameter("e", r.getEmail());
        int status = q.executeUpdate();
        t.commit();
        session.close();
        return status;
    }
    
    public static List login(Registration r)
    {
        session = HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        Query q=session.createQuery("select password from Registration where email=:e");
        q.setParameter("e", r.getEmail());
        List list = q.list();
        t.commit();
        session.close();
        return list;
    }
    
    public static List Search(Registration r)
    {
        session=HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        Query q=session.createQuery("select cancerType from Registration where email=:e");
        q.setParameter("e", r.getEmail());
        List list=q.list();
        session.close();
        return list;
    }
}
