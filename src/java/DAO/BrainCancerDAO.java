/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import static DAO.BloodCancerDAO.session;
import Utils.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Transaction;
import pojos.Braincancerdoctors;
import pojos.Mouthcancerdoctors;

/**
 *
 * @author Harsh Jasani
 */
public class BrainCancerDAO {
    public static List<Braincancerdoctors> Search()
    {
        session=HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        Query q=session.createQuery("from Braincancerdoctors");
        List<Braincancerdoctors> list = q.list();
        t.commit();
        session.close();
        return list;
    }
}
