/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Utils.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojos.Registration;
import pojos.Bloodcancerdoctors;

/**
 *
 * @author Harsh Jasani
 */
public class BloodCancerDAO {
    
    static Session session=null;
    
    public static void save(Bloodcancerdoctors d)
    {
        session=HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        session.save(d);
        t.commit();
        session.close();
    }
    
    public static List<Bloodcancerdoctors> Search()
    {
        session=HibernateUtil.getSessionFactory().openSession();
        Transaction t=session.beginTransaction();
        Query q=session.createQuery("from Bloodcancerdoctors");
        List<Bloodcancerdoctors> list = q.list();
        t.commit();
        session.close();
        return list;
    }
}
