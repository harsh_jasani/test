
<%@page import="java.util.List"%>
<%@page import="DAO.RegistationDAO"%>
<%@page import="pojos.Registration" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String email = request.getParameter("email");
    Registration p=new Registration();
    p.setEmail(email);
    List l = RegistationDAO.login(p);
    if(l.isEmpty())
    {
    }
    else
    {
        session.setAttribute("email", email);
        RequestDispatcher d=request.getRequestDispatcher("index.jsp");
        d.forward(request, response);
    }
    
%>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
    <!-- Main css -->
    <link rel="stylesheet" href="css/style1.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    
</head>
<body>
    <div class="main">
        <section class="signup">
            <div class="container">
                <div class="signup-content">
                    <form  name="form" id="signup-form" class="signup-form" method="post" action="SaveServlet">
                        <h2 class="form-title">Create account</h2>
                        <div class="form-group">
                            <input type="text" class="form-input" name="name" id="name"  required/>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-input" name="email" id="email"  required/>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-input" name="password" id="password" placeholder="Password" required/>
                            <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-input" name="re_password" id="re_password" placeholder="Repeat your password" required/>
                        </div>
                        <div class="form-group">
                            <input class="form-input"  type="text"  name="city" placeholder="Enter The City" required>
                        </div>   
                        <div class="form-group">
                            <select class="form-input" name="cancerType">
                                <option value="Brain Cancer">Brain Cancer</option>
                                <option value="Blood Cancer">Blood Cancer</option>
                                <option value="Bone Cancer">Bone Cancer</option>
                                <option value="Lung Cancer">Lung Cancer</option>
                                <option value="Mouth Cancer">Mouth Cancer</option>
                            </select>
                        </div> 
                        <div class="form-group">
                            <input class="form-input"  type="date" required name="date">
                        </div>        
                        <div class="form-group">
                            <textarea  class="form-input"  type="text" name ="address" placeholder="Address" required></textarea>
                        </div>
                         <div class="form-group">
                             <input type="text" class="form-input" name="mobile" id="mobile" placeholder="Mobile Number" required />
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" id="submit" class="form-submit" value="Sign up" onclick='phonenumber()'/>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>

    <!-- JS -->
    <script>
    function phonenumber()
    {
    var password = document.getElementById("password").value;
    var repassword = document.getElementById("re_password").value;
    var mobileVal=document.getElementById("mobile").value;
    if(password==repassword)
    {
         if(mobileVal.length===10)
         {
             return true;
          }
      else
  {
    alert("Not a valid Phone Number");
   return false;
   }
    }
    else
    {
        alert("Password and repassword is Not Same");
        return false;
    }
   
}
    </script>
    <script>
        var email = localStorage.getItem("email");
        var name=localStorage.getItem("name");
        if(email==null)
        {
            document.getElementById("email").value="Enter Your Email";
            document.getElementById("name").value="Enter Your Name";
        }
        else
        {
            document.getElementById("email").value=email;
            document.getElementById("name").value=name;
        }
        
    </script>
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="js/main1.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
</body>
</html>
