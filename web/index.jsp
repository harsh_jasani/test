<%@page import="DAO.BloodCancerDAO"%>
<%@page import="pojos.Bloodcancerdoctors"%>
<%
    String log="";
    String url="";
    String email=(String)session.getAttribute("email");
    if( email!=null)
    {     
       log="Logout";
       url="Logout.jsp";
    }
    else
    {
        log="login";
        url="Login.html";
    }
%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>About Cancer</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900"> 
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css1/bootstrap.min.css">
    <link rel="stylesheet" href="css1/magnific-popup.css">
    <link rel="stylesheet" href="css1/jquery-ui.css">
    <link rel="stylesheet" href="css1/owl.carousel.min.css">
    <link rel="stylesheet" href="css1/owl.theme.default.min.css">
    <link rel="stylesheet" href="css1/bootstrap-datepicker.css"> 
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="css1/aos.css">
    <link rel="stylesheet" href="css1/style.css">
  </head>
  <body>
  <div class="site-wrap">
    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div> <!-- .site-mobile-menu -->
    
    
    <div class="site-navbar-wrap">
      <div class="site-navbar-top">
        <div class="container py-2">
          <div class="row align-items-center">
           
            
          </div>
        </div>
      </div>
      <div class="site-navbar">
        <div class="container py-1">
          <div class="row align-items-center">
            <div class="col-2">
              <h2 class="mb-0 site-logo"><a href="index.html"> Cancer</a></h2>
            </div>
            <div class="col-10">
              <nav class="site-navigation text-right" role="navigation">
                <div class="container">
                  <div class="d-inline-block d-lg-none ml-md-0 mr-auto py-3"><a href="#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

                  <ul class="site-menu js-clone-nav d-none d-lg-block">
                    <li><a href="index.jsp">Home</a></li>
                    <li><a href="about.html">About Us</a></li>
                    <li class="has-children">
                    <a href="">Cancer Type</a>
                        <ul class="dropdown arrow-collapse">
                            <li><a  onclick="location.href='cancerDetail.jsp?id=1'">Brain Cancer</a></li>
                            <li><a  onclick="location.href='cancerDetail.jsp?id=2'">Blood Cancer</a></li>
                           <li><a   onclick="location.href='cancerDetail.jsp?id=3'">Bone Cancer</a></li>
                           <li><a   onclick="location.href='cancerDetail.jsp?id=4'">Lung Cancer</a></li>
                           <li><a   onclick="location.href='cancerDetail.jsp?id=5'">Mouth Cancer</a></li>
                      </ul>
                    </li>    
                     <li><a href=<%=url%> onclick="On();"><%= log%></a></li>
                    <li><a href="contact.html">Contact</a></li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="site-blocks-cover" style="background-image: url(images/hero_bg_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-7">
            <span class="sub-text">We Priority Your</span>
            <h1>Your <strong>New Smile</strong></h1>
          </div>
        </div>
      </div>
    </div>  

    <div class="site-block-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="site-block-feature d-flex p-4 rounded mb-4">
              <div class="mr-3">
                <span class="icon flaticon-tooth font-weight-light text-white h2"></span>
              </div>
              <div class="text">
                <h3>Doctors</h3>
                <p>The life so short, the craft so long to learn</p>
              </div>
            </div>   
          </div>
          <div class="col-lg-4">
            <div class="site-block-feature d-flex p-4 rounded mb-4">
              <div class="mr-3">
                <span class="icon flaticon-tooth-whitening font-weight-light text-white h2"></span>
              </div>
              <div class="text">
                <h3>exercise</h3>
                <p>To enjoy the glow of good health, you must exercise</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="site-block-feature d-flex p-4 rounded mb-4">
              <div class="mr-3">
                <span class="icon flaticon-tooth-pliers font-weight-light text-white h2"></span>
              </div>
              <div class="text">
                <h3>Medicine</h3>
                <p>Always laugh when you can, it is cheap medicine</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section site-block-3">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5 mb-lg-0">
            <img src="images/img_1.jpg" alt="Image" class="img-fluid">
          </div>
          <div class="col-lg-6">
            <div class="row row-items">
              <div class="col-md-6">
                <a href="#" class="d-flex text-center feature active p-4 mb-4">
                  <span class="align-self-center w-100">
                    <span class="d-block mb-3">
                      <span class="flaticon-tooth-whitening display-3"></span>
                    </span>
                    <h3>Doctors</h3>
                  </span>
                </a>
              </div>
              <div class="col-md-6">
                <a href="#" class="d-flex text-center feature p-4 mb-4">
                  <span class="align-self-center w-100">
                    <span class="d-block mb-3">
                      <span class="flaticon-stethoscope display-3"></span>
                    </span>
                    <h3>Health Care</h3>
                  </span>
                </a>
              </div>
            </div>
            <div class="row row-items last">
              <div class="col-md-6">
                <a href="#" class="d-flex text-center feature p-4 mb-4">
                  <span class="align-self-center w-100">
                    <span class="d-block mb-3">
                      <span class="flaticon-first-aid-kit display-3"></span>
                    </span>
                    <h3>Exercise</h3>
                  </span>
                </a>
              </div>
              <div class="col-md-6">
                <a href="#" class="d-flex text-center active feature p-4 mb-4">
                  <span class="align-self-center w-100">
                    <span class="d-block mb-3">
                      <span class="flaticon-tooth-pliers display-3"></span>
                    </span>
                    <h3>Medicine</h3>
                  </span>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="site-section site-block-appointment">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5 pl-lg-5 order-1 order-lg-2">
            <div class="pl-lg-5 ml-lg-5">
              <h2 class="site-heading text-black"><strong>Exercise</strong> </h2>
              <p class="lead text-black">Change is not something that we should fear. Rather, it is something that we should welcome. For without change, nothing in this world would ever grow or blossom, and no one in this world would ever move forward to become the person they?re meant to be</p>
              <a href="Eexercise.jsp">Read More</a>
            </div>
          </div>
          <div class="col-lg-6 order-2 order-lg-1">
			<image src="images/yoga3.jpg" width="600" height="500"></image>          
          </div>
        </div>
      </div>
    </div>
      <div class="site-section site-block-appointment c">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5 pl-lg-5 order-1 order-lg-2">
            <div class="pl-lg-5 ml-lg-5">
              <h2 class="site-heading text-black"><strong>Doctors</strong></h2>
              <p class="lead text-black">let me congratulate you on the choice of calling which offers a combination of intellectual and moral interests found in no other profession.</p>
              <a href="ckeckcancertype.jsp">Read More</a>
            </div>
          </div>
          <div class="col-lg-6 order-2 order-lg-1">
			<image src="images/doctor.jpg" width="600" height="500"></image>          
          </div>
        </div>
      </div>
    </div>
         
         
    <div class="site-block-half d-block d-lg-flex site-block-video">
      <div class="image bg-image order-2" style="background-image: url(images/hero_bg_1.jpg); ">
        <a href="https://vimeo.com/channels/staffpicks/93951774" class="play-button popup-vimeo"><span class="icon-play"></span></a>
      </div>
      <div class="text order-1">
        <h2 class="site-heading text-black mb-3">Success <strong>Stories</strong></h2>
        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id cum vel, eius nulla inventore aperiam excepturi molestias incidunt, culpa alias repudiandae, a nobis libero vitae repellendus temporibus vero sit natus.</p>
        <p>Dolores perferendis ipsam animi fuga, dolor pariatur aliquam esse. Modi maiores minus velit iste enim sunt iusto, dolore totam consequuntur incidunt illo voluptates vero quaerat excepturi. Iusto dolor, cum et.</p>
      </div>
    </div>
    <div class="site-section bg-light">
      <div class="container">
        <div class="row mb-5 justify-content-center">
          <div class="col-md-6 text-center">
            <h2 class="site-heading text-black">People <strong>Says</strong></h2>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4">
            <div class="site-block-testimony p-4 text-center">
              <div class="mb-3">
                <img src="images/person_2.jpg" alt="Image" class="img-fluid">
              </div>
              <div>
                 <p>Dolores perferendis ipsam animi fuga, dolor pariatur aliquam esse. Modi maiores minus velit iste enim sunt iusto dolore</p>
                 <p><strong class="font-weight-bold">Nathalie Oscar</strong></p>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="site-block-testimony p-4 text-center active">
              <div class="mb-3">
                <img src="images/person_1.jpg" alt="Image" class="img-fluid">
              </div>
              <div>
                 <p>Dolores perferendis ipsam animi fuga dolor pariatur aliquam esse. Modi maiores minus velit iste enim sunt iusto dolore</p>
                 <p><strong class="font-weight-bold">Linda Uler</strong></p>
              </div>
            </div>
          </div>
          
          <div class="col-lg-4">
            <div class="site-block-testimony p-4 text-center">
              <div class="mb-3">
                <img src="images/person_3.jpg" alt="Image" class="img-fluid">
              </div>
              <div>
                 <p>Dolores perferendis ipsam animi fuga dolor pariatur aliquam esse. Modi maiores minus velit iste enim sunt iusto dolore</p>
                 <p><strong class="font-weight-bold">Chris Coodard</strong></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer class="site-footer border-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="row mb-5">
              <div class="col-md-12">
                <h3 class="footer-heading mb-4">Navigation</h3>
              </div>
              <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Services</a></li>
                  <li><a href="#">News</a></li>
                  <li><a href="#">Team</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled">
                  <li><a href="#">About Us</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Contact Us</a></li>
                  <li><a href="#">Membership</a></li>
                </ul>
              </div>
            </div>

            
          </div>
          <div class="col-lg-4">
            <div class="mb-5">
              <h3 class="footer-heading mb-4">Recent News</h3>
              <div class="block-25">
                <ul class="list-unstyled">
                  <li class="mb-3">
                    <a href="#" class="d-flex">
                      <figure class="image mr-4">
                        <img src="images/hero_bg_1.jpg" alt="" class="img-fluid">
                      </figure>
                      <div class="text">
                        <span class="small text-uppercase date">Sep 16, 2018</span>
                        <h3 class="heading font-weight-light">Lorem ipsum dolor sit amet consectetur elit</h3>
                      </div>
                    </a>
                  </li>
                  <li class="mb-3">
                    <a href="#" class="d-flex">
                      <figure class="image mr-4">
                        <img src="images/hero_bg_1.jpg" alt="" class="img-fluid">
                      </figure>
                      <div class="text">
                        <span class="small text-uppercase date">Sep 16, 2018</span>
                        <h3 class="heading font-weight-light">Lorem ipsum dolor sit amet consectetur elit</h3>
                      </div>
                    </a>
                  </li>
                  <li class="mb-3">
                    <a href="#" class="d-flex">
                      <figure class="image mr-4">
                        <img src="images/hero_bg_1.jpg" alt="" class="img-fluid">
                      </figure>
                      <div class="text">
                        <span class="small text-uppercase date">Sep 16, 2018</span>
                        <h3 class="heading font-weight-light">Lorem ipsum dolor sit amet consectetur elit</h3>
                      </div>
                    </a>
                  </li>
                </ul>
              </div>
            </div>        
          </div>
          <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="mb-5">
              <h3 class="footer-heading mb-2">Subscribe Newsletter</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit minima minus odio.</p>

              <form action="#" method="post">
                <div class="input-group mb-3">
                  <input type="text" class="form-control border-white text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary" type="button" id="button-addon2">Send</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="row">
              <div class="col-md-12">
                <h3 class="footer-heading mb-4">Follow Us</h3>
                <div>
                  <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                  <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                  <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                  <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
                </div>
              </div>
            </div>


          </div>
          
        </div>
    </footer>
  </div>
  <script>
      function On()
      {
         if(localStorage.getItem("key")!=null)
       {
        gapi.auth2.getAuthInstance().disconnect();
        localStorage.removeItem("key");
        localStorage.removeItem("email");
        localStorage.removeItem("name");
        location.reload();
       }
      }
      
  </script>
  <script src="js1/jquery-3.3.1.min.js"></script>
  <script src="js1/jquery-migrate-3.0.1.min.js"></script>
  <script src="js1/jquery-ui.js"></script>
  <script src="js1/popper.min.js"></script>
  <script src="js1/bootstrap.min.js"></script>
  <script src="js1/owl.carousel.min.js"></script>
  <script src="js1/jquery.stellar.min.js"></script>
  <script src="js1/jquery.countdown.min.js"></script>
  <script src="js1/jquery.magnific-popup.min.js"></script>
  <script src="js1/bootstrap-datepicker.min.js"></script>
  <script src="js1/aos.js"></script>

  <script src="js1/main.js"></script>
    
  </body>
</html>